package th.co.extosoft.app.ws;

/**
 * Generated interface, please do not edit.
 * Date: [Fri Jan 04 16:06:19 ICT 2019]
 */

public interface AppTierWS2PortType extends java.rmi.Remote {

  /**
   * Web Method: callDeleteConfig ...
   */
  java.lang.String callDeleteConfig(th.co.extosoft.app.itf.ConfigurationRequest configReqs)
      throws java.rmi.RemoteException;
  /**
   * Web Method: callConfigurationResponse ...
   */
  th.co.extosoft.app.itf.ConfigurationResponse callConfigurationResponse()
      throws java.rmi.RemoteException;
  /**
   * Web Method: callUpdateConfig ...
   */
  java.lang.String callUpdateConfig(th.co.extosoft.app.itf.ConfigurationRequest configReqs)
      throws java.rmi.RemoteException;
  /**
   * Web Method: callGetConfig ...
   */
  java.util.List callGetConfig(th.co.extosoft.app.itf.ConfigurationRequest configReqs)
      throws java.rmi.RemoteException;
  /**
   * Web Method: callAddConfig ...
   */
  java.lang.String callAddConfig(th.co.extosoft.app.itf.ConfigurationRequest configReqs)
      throws java.rmi.RemoteException;
}
