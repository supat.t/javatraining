package th.co.extosoft.app.ws;

/**
 * Generated interface, please do not edit.
 * Date: [Fri Jan 04 16:06:19 ICT 2019]
 */

public interface AppTierWSPortType extends java.rmi.Remote {

  /**
   * Web Method: callAddData ...
   */
  void callAddData()
      throws java.rmi.RemoteException;
  /**
   * Web Method: callGetData ...
   */
  void callGetData()
      throws java.rmi.RemoteException;
  /**
   * Web Method: callDeleteData ...
   */
  void callDeleteData()
      throws java.rmi.RemoteException;
  /**
   * Web Method: callUpdateData ...
   */
  void callUpdateData()
      throws java.rmi.RemoteException;
}
