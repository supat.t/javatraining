package th.co.extosoft.app.ws;

import java.util.List;

import javax.jws.*;

import th.co.extosoft.app.biz.AppTierBusiness;
import th.co.extosoft.app.itf.ConfigurationRequest;
import th.co.extosoft.app.itf.ConfigurationResponse;

@WebService
public class AppTierWS2 {

	@WebMethod
	public List<ConfigurationResponse> callGetConfig(ConfigurationRequest configReqs) {
		
		AppTierBusiness appTierBusiness = new AppTierBusiness();
	//	ConfigurationRequest configReqs = new ConfigurationRequest();
		List<ConfigurationResponse> configResp = null;
		configResp = appTierBusiness.callGetConfig(configReqs);

		return configResp;
	}
	@WebMethod
	public String callAddConfig(ConfigurationRequest configReqs) {
		String Msg = null;
		AppTierBusiness appTierBusiness = new AppTierBusiness();
	//	ConfigurationRequest configReqs = new ConfigurationRequest();
		List<ConfigurationResponse> configResp = null;
		Msg = appTierBusiness.callAddConfig(configReqs);

		return Msg;
	}
	@WebMethod
	public String callUpdateConfig(ConfigurationRequest configReqs) {
			String Msg = null;
			AppTierBusiness appTierBusiness = new AppTierBusiness();
		//	ConfigurationRequest configReqs = new ConfigurationRequest();
			List<ConfigurationResponse> configResp = null;
			Msg = appTierBusiness.callUpdateConfig(configReqs);

			return Msg;
	}
	@WebMethod
	public String callDeleteConfig(ConfigurationRequest configReqs) {
		String Msg = null;
		AppTierBusiness appTierBusiness = new AppTierBusiness();
	//	ConfigurationRequest configReqs = new ConfigurationRequest();
		List<ConfigurationResponse> configResp = null;
		Msg = appTierBusiness.callDeleteConfig(configReqs);

		return Msg;
}
	
	@WebMethod
	 public ConfigurationResponse callConfigurationResponse() {
	  return new ConfigurationResponse();
	 }
	
}