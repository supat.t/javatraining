# JavaTraining

 Java Training Course 3-Jan-2019 to 5-Jan-2019
Agenda
1) Java Programming
2) Java connect to MySQL
3) Create Java Web Service (API) with Method GET, POST, Etc.
4) Create JSON call Web Service (API)
5) How to deploy .war on Tomcat
6) Setup Web Tier an